import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage

    Image {
        source: "./background.png"
        anchors {
            right: parent.right
            bottom: parent.bottom
        }
        width: parent.width * 4/5
        fillMode: Image.PreserveAspectFit
        opacity: 0.2
    }

    CoverPlaceholder {
        text: Qt.application.name
        textColor: Theme.highlightColor
        CoverActionList {
            CoverAction { iconSource: "image://theme/icon-m-sync";            onTriggered: {app.sheepModel.getNewData} }
            CoverAction { iconSource: "image://theme/icon-m-night";           onTriggered: {app.startRenderJob()} }
            CoverAction { iconSource: "image://theme/icon-m-media-playlists"; onTriggered: {pageStack.push(Qt.resolvedUrl("VideoPage.qml"), {"playlist": sheepModel.getPlayList(), "generation": sheepModel.gen, "imgid": qsTr("all")} ) } }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
