import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import MeeGo.Connman 0.2
import "cover"
import "pages"
import "javascript/esapi.js" as API

ApplicationWindow {
    id: app

    property alias online: onlineHelper.online
    property bool offline: !online

    property ConfigurationGroup settings: settingsGroup
    property ConfigurationGroup sheepdata: sheepData
    property ConfigurationGroup jobdata: jobData
    //readonly property string playlistFile: StandardPaths.data + "/sheep.xml"

    // TODO: make selectServer actually work:
    // for now the load balancer always returns these anyway:
    property string qserver: "http://v3d0.sheepserver.net/"
    property string vserver: "http://v3d0.sheepserver.net/"
    property string rserver: "http://v3d0.sheepserver.net/"

    // rate-limit queries
    property int queried: 0
    property int retry: 600
    property bool mayRequest: !backoffTimer.running
    /*
    property int timeLeft: Math.floor(retry / 100)
    Timer {
        id: backoffTimeLeft
        interval: 1000
        running: backoffTimer.running
        onTriggered: {
            app.timeLeft = Math.floor((retry - 100) / 100)
        }
    }
    */
    Timer {
        id: backoffTimer
        running: false
        repeat: false
        interval: retry * 1000
    }

    // initialize things
    Component.onCompleted: {
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
    }

    // handle network events
    /*
    onOnlineChanged: {
        if (online) {
            console.debug("went online");
            if (settings.qserver <= 0) {
                API.selectQServer(settings.account, settings.hash, Qt.application.version, settings.appid);
            }
        } else {
            console.debug("went offline");
        }
    }
    */

    ConfigurationGroup  {
        id: settingsGroup
        path: "/org/nephros/" + Qt.application.name
        property bool hires: false
        property string appid: ""
        property string hash: ""
        property string account: ""
        Component.onCompleted: {
            // generate the app UUID on (first) launch:
            if (appid.length != 16) { genAppId(); }
        }

        // create an app Id for requests, also used in SettingsPage
        function genAppId() {
            var uuid = Qt.md5(Qt.application.name + Qt.application.version + Date.now().toString()).substr(0,16);
            setValue("appid", uuid);
            console.info("Generated new App ID:" + uuid);
            sync();
        }
        // hash the passsword and save:
        function saveHash(p,u) {
            var salt = 'sh33p';
            var h = Qt.md5(p + u + salt);
            console.info("Generated new hash");
            setValue("hash", h);
            sync();
        }
    }
    ConfigurationGroup  {
        id: sheepData
        scope: settingsGroup
        path:  "sheepdata"
        property int generation: 0
        property string rawdata: ""
        property string playlist: ""
    }
    ConfigurationGroup  {
        id: jobData
        scope: settingsGroup
        path:  "jobs"
        property int jobId: 0
        property string parameters: ""
        property string result: ""
    }
    NetworkManager {
        id: onlineHelper
        readonly property bool online: state == "online" || state == "ready"
    }
    Component {
        id: wpImage
        Image {
            // silence some warnings
            property url imageUrl: source
            property string wallpaperFilter: ""
            source: Qt.resolvedUrl("http://www.storytrender.com/wp-content/uploads/2018/03/12_CATERS_sea_slug_sheep_004-1024x768.jpg")
            asynchronous: true
            fillMode: Image.PreserveAspectFit
        }
    }
    background.wallpaper: wpImage
    //background.image: Qt.resolvedUrl("http://www.storytrender.com/wp-content/uploads/2018/03/12_CATERS_sea_slug_sheep_004-1024x768.jpg")
    cover: Component { CoverPage{} }
    initialPage: Component { MainPage{} }
}

 // vim: ft=javascript expandtab ts=4 sw=4 st=4
