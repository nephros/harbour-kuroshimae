/* global */

var qserver;
var rserver;
var vserver;

function selectQServer(account, hash, version, uid) {
    if (qserver) {
        return qserver;
    } else {
        selectServer(account, hash, version, uid);
    }
    console.debug("Returning server", qserver);
}
function selectVServer(account, hash, version, uid) {
    if (vserver) {
        return vserver;
    } else {
        selectServer(account, hash, version, uid);
    }
    console.debug("Returning server", vserver);
}
function selectRServer(account, hash, version, uid) {
    if (rserver) {
        return rserver;
    } else {
        selectServer(account, hash, version, uid);
    }
    console.debug("Returning server", rserver);
}
/*
 * the API sais we first query for a list of servers to query:
 *
 * "http://community.sheepserver.net/query.php?q=redir&u=account&p=hash&v=version&i=uid)
 */
function selectServer( account, hash, version, uid) {

    //var fileUrl = "http://community.sheepserver.net/query.php?q=redir&u=" + account + "&p=" + hash + "&v=" + version "&i=" + uid)
    //var requestUrl = "http://community.sheepserver.net/query.php?q=redir";
    var requestUrl = "http://community.sheepserver.net/query.php";
    var query = Qt.resolvedUrl(requestUrl);
    query = query + "?q=redir";
    query = query + "&u=" + encodeURIComponent(account);
    query = query + "&p=" + encodeURIComponent(hash);
    query = query + "&v=" + encodeURIComponent(version);
    query = query + "&i=" + encodeURIComponent(uid);
    console.debug("Query string:", query);

    var ret;
    var r = new XMLHttpRequest();
    r.open('GET', query); // false = synchronous request
    //r.open('GET', query, false); // false = synchronous request
    r.setRequestHeader('User-Agent', "Mozilla/5.0 (Sailfish 4.0; Mobile) " + Qt.application.name + "/" + Qt.application.version);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    r.send();

    r.onreadystatechange = function(event) {
        if (r.readyState == XMLHttpRequest.DONE) {
            // <query><redir role="none" host="http://v3d0.sheepserver.net/"
            // vote="http://v3d0.sheepserver.net/"
            // render="http://v3d0.sheepserver.net/"/></query>
            if (r.status === 200 || r.status == 0) {

            console.debug("pure Response servers is " + r.response);

            var re = /host="([^"]+)/gi;
            var servers = re.exec(r.response);
            setServer("q", servers[1]);
            console.debug(" Response servers is Q " + servers[1]);

            re = /vote="([^"]+)/gi;
            servers = re.exec(r.response);
            setServer("v", servers[1]);
            console.debug(" Response servers is V " + servers[1]);

            re = /render="([^"]+)/gi;
            servers = re.exec(r.response);
            setServer("r", servers[1]);
            console.debug(" Response servers is R " + servers[1]);

            } else {
                console.debug("error in processing request.");
            }
        }
    }
}

function setServer(which, what) {
    console.debug(" Saving server", which, what);
    if (which === "q")
        qserver = what;
    if (which === "v")
        vserver = what;
    if (which === "r")
        rserver = what;
}

function getStats(id) {
    var r = getDetail(id, "stats");
    // ... parse HTML table ;)
}

function getDetail(id, what) {
    // http://v3d0.sheepserver.net/cgi/node.cgi?id=63404&detail=stats
    //
    var requestUrl = qserver + "/cgi/node.cgi"
    var query = Qt.resolvedUrl(requestUrl);
    query = query + "?id=" + id;
    query = query + "&detail=" + what;
    console.debug("Query string:", query);

    var ret;
    var r = new XMLHttpRequest();
    r.open('GET', query, true); // synchronous request
    r.setRequestHeader('User-Agent', "Mozilla/5.0 (Sailfish 4.0; Mobile) " + Qt.application.name + "/" + Qt.application.version);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    r.send();

    r.onreadystatechange = function(event) {
        if (r.readyState == XMLHttpRequest.DONE) {
            ret = r.response;
        }
    }
    return ret;
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
