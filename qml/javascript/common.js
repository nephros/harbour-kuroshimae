.import "./gunzip-min.js" as GZ

/*
 *
 */

function unzipResponse(data) {
    /*
     * combined solution from:
     * - https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Sending_and_Receiving_Binary_Data
     * - http://dougbtv.com/2014/04/16/decompressing-gzipped-with-javascript/
     */
    var arrayBuffer = data;
    console.time("Decompressed in" );
    var bytes = [];
    if (arrayBuffer) {
        var byteArray = new Uint8Array(arrayBuffer);
        for (var i = 0; i < byteArray.byteLength; i++) {
            var abyte = byteArray[i] & 0xff;
            bytes.push(abyte);
            //console.debug(i + "/" + byteArray.byteLength);
        }
    }

    // Instantiate our zlib object, and gunzip it.
    // Requires: http://goo.gl/PIqhbC [github]
    // (remove the map instruction at the very end.)
    var  gunzip  =  new  GZ.Zlib.Gunzip ( bytes );
    var  plain  =  gunzip.decompress ();

    // Now go ahead and create an ascii string from all those bytes.
    var asciistring = "";
    for (var i = 0; i < plain.length; i++) {
        asciistring += String.fromCharCode(plain[i]);
    }
    console.timeEnd("Decompressed in" );
    return asciistring;
}

// sort array by two fields:
// https://medium.com/developer-rants/sorting-json-structures-by-multiple-fields-in-javascript-60ed96704df7
function sortSheepByLast(what) {
    var sheep = what;
    sheep = sheep.sort(function(a, b) {
        var retval = 0;
        if (a.last > b.last)
            retval = -1;
        if (a.last < b.last)
            retval = 1;
        if (retval === 0)
            retval = a.sheep < b.sheep ? -1 : 1;
        return retval;
    });
}

// sort array what by column by
function sortByColumn(what,by) {
    var arr = what;
    arr = arr.sort(function(a,b) {
        return a[by] - b[by];
    })
    return arr;
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
