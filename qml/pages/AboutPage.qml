import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    id: aboutPage
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: aboutColumn.height

        VerticalScrollDecorator {}

        Column {
            id: aboutColumn
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge
            bottomPadding: Theme.itemSizeMedium

            PageHeader { title: qsTr("About") + " " + Qt.application.name + " " + Qt.application.version }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                source: Qt.resolvedUrl("../images/icon.svg")
                sourceSize.width: parent.width /3
                width: sourceSize.width
                fillMode: Image.PreserveAspectFit
            }

            SectionHeader { text: qsTr("What's %1?").arg(Qt.application.name) }
            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignJustify
                text: qsTr("%1 is an unofficial ElectricSheep client for Sailfish OS.").arg(Qt.application.name)
            }

            SectionHeader { text: qsTr("What's an Electric Sheep?") }
            AboutLabel {
                horizontalAlignment: Text.AlignJustify
                link: Qt.resolvedUrl("https://electricsheep.org")
                text: qsTr("<p>Electric Sheep is a collaborative abstract artwork run by thousands of people all over the world. When their computers 'sleep', the Electric Sheep comes on and the computers communicate with each other by the internet to share the work of creating morphing abstract animations known as 'sheep'.</p> ")
            }
            AboutLabel {
                link: Qt.resolvedUrl("https://electricsheep.org")
                text: qsTr("<p><a href='%1'>electricsheep.org</a>").arg(link.toString())
            }
            SectionHeader { text: qsTr("Why the name?") }
            AboutLabel {
                horizontalAlignment: Text.AlignJustify
                text: qsTr("So, this is an image of <i>Costasiella kuroshimae</i>, a mollusc who lives in the sea:")
            }

            Loader {
                anchors.horizontalCenter: parent.horizontalCenter
                sourceComponent: kuroimage
                asynchronous: true
                active: aboutPage.visible
                // to avoid binding loop
                property var parentWidth: visible ? parent.width : 0
            }
            Component {
                id: kuroimage
                Image {
                    source: Qt.resolvedUrl("http://www.storytrender.com/wp-content/uploads/2018/03/12_CATERS_sea_slug_sheep_004-1024x768.jpg")
                    sourceSize.width: parent.parentWidth - Theme.itemSizeSmall
                    width: sourceSize.width
                    fillMode: Image.PreserveAspectFit
                    BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(parent.source) }
                }
            }


            SectionHeader { text: qsTr("Licenses") }
            AboutLabel {
                link: Qt.resolvedUrl("https://www.apache.org/licenses/LICENSE-2.0.html");
                text: "<p>" + qsTr("%1 is free software released under the <a href='%2'>Apache License 2.0</a>.").arg(Qt.application.name).arg(link.toString()) + "</p>"
            }
            AboutLabel {
                text: "<p>" + qsTr("%1 uses components licensed under MIT and LGPL 2.1 licenses</a>.").arg(Qt.application.name).arg(link.toString()) + "</p>"
            }
            ReUseTag {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                textFormat: Text.StyledText
            }
            AboutLabel {
                property url link: Qt.resolvedUrl("http://creativecommons.org/licenses/by-nc/3.0/us/");
                text: qsTr("<a href='%1'>Creative Commons By-NC 3.0</a>").arg(link.toString())
            }

            SectionHeader { text: qsTr("Source Code") }
            AboutLabel {
                link: Qt.resolvedUrl("https://gitlab.com/nephros/harbour-kuroshimae");
                text: qsTr("Sources for %1 <a href='%2'>available on GitLab</a>").arg(Qt.application.name).arg(link.toString())
            }
            AboutLabel {
                link: Qt.resolvedUrl("https://github.com/scottdraves/");
                text: qsTr("Sources for the original %1 client and the fractal flame renderer <a href='%2'>available on Github</a>").arg(qsTr("Electric Sheep")).arg(link.toString())
            }

            SectionHeader { text: qsTr("Developers") }
            AboutLabel {
                text: Qt.application.name
                        + ": <a href='"
                        + encodeURI("mailto:sailfish+kuroshimae@nephros.org?"
                            + "&subject=A message from a " + Qt.application.name + " user"
                            + "&body=Hello nephros, "
                        )
                        + "'>Peter G. (nephros)</a>"
            }
            AboutLabel {
                //link: Qt.resolvedUrl("https://scottdraves.com");
                link: Qt.resolvedUrl("https://electricsheep.org/credits/");
                text: qsTr("%1 by <a href='%2'>Scott Draves and others</a>").arg(qsTr("Electric Sheep")).arg(link.toString())
            }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
