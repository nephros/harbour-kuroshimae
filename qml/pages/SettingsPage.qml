import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    SilicaFlickable{
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            spacing: Theme.paddingSmall
            bottomPadding: Theme.itemSizeLarge
            width: parent.width - Theme.horizontalPageMargin
            PageHeader{ title:  Qt.application.name + " " + qsTr("Settings", "page title") }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Application")
            }
            TextSwitch{
                id: qualSwitch
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.settings.hires
                automaticCheck: true
                text: qsTr("Prefer High-Res Videos")
                description: qsTr("Download larger and high quality videos.")
                onClicked: app.settings.hires = checked
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Account")
            }
            TextField{
                id: accountField
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                placeholderText: qsTr("Account Name")
                text: app.settings.account
                acceptableInput: text.length > 0
                EnterKey.enabled: text.length > 0
                EnterKey.onClicked: focus = false
                onFocusChanged: {
                    if (acceptableInput && !focus) {
                        app.settings.setValue("account", text)
                    }
                }
             }
             Label{
                width: parent.width - Theme.horizontalPageMargin
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignJustify
                text: qsTr("Account credentials are not saved, only a salted hash is. Still, this is insecure, so don't use valuable credentials here.")
            }
            TextSwitch{
                id: credSwitch
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                checked: app.settings.hash != ""
                automaticCheck: true
                text: qsTr("Already Configured")
                description: qsTr("Tap this switch to change username and password.")
                //onClicked: config.foo = checked
            }
            TextField{
                id: usernameField
                enabled: !credSwitch.checked
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                placeholderText: qsTr("Username")
                acceptableInput: text.length > 0
                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: passwordField.focus = true
            }
            PasswordField{
                id: passwordField
                enabled: (!credSwitch.checked && usernameField.acceptableInput)
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                showEchoModeToggle: true
                inputMethodHints: Qt.ImhNoPredictiveText
                passwordEchoMode: TextInput.PasswordEchoOnEdit
                acceptableInput: text.length > 3
                EnterKey.enabled: acceptableInput
                EnterKey.onClicked: passwordField.focus = false
                onFocusChanged: {
                    if (acceptableInput && !focus) {
                        app.settings.saveHash(usernameField.text, passwordField.text)
                    }
                }
            }
            SectionHeader {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Advanced")
            }
            Label{
                width: parent.width - Theme.horizontalPageMargin
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.Wrap
                horizontalAlignment: Text.AlignJustify
                text: qsTr("The App ID is required by the Electric Sheep web services. It is supposed to be generated at application install only, but for the sake of privacy, you can reset it here. This shouldn't ususally be necessary though, and the effects of doing so are unknown.")
            }
            TextField {
                id: idField
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                placeholderText: app.settings.appid ? app.settings.appid : ""
                label: qsTr("App Id")
                labelVisible : true
                softwareInputPanelEnabled: false
                rightItem: IconButton{
                    icon.source: "image://theme/icon-m-refresh"
                    onClicked: Remorse.popupAction(this, qsTr("Regenerated %1").arg(idField.label), function() { app.settings.genAppId() } )
                }
            }
            /*
            Slider{
                id: memSlider
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Cache Memory")
                minimumValue: 0
                maximumValue: 10
                stepSize: 0.5
                value: app.mem ? app.mem : -1
                valueText: {
                    if (sliderValue === 0) {
                        return qsTr("automatic");
                    } else {
                        return Math.round(sliderValue * 12.8) + qsTr("MB");
                    }
                }
                onReleased: app.mem = Math.round(sliderValue)
            }
            Label{
                anchors {
                    // align to slider left
                    left: memSlider.left
                    leftMargin: memSlider.leftMargin //+ Theme.paddingLarge
                    right: memSlider.right
                    rightMargin: memSlider.rightMargin //+ Theme.paddingLarge
                    topMargin: Theme.paddingMedium
                }
                width: parent.width // - Theme.paddingLarge
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.Wrap
                text: qsTr("Restart the App to apply changes.")
            }
            */
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
