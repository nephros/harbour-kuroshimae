import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Gallery 1.0
import "../components"

Page {
    id: mainPage

    // initialize things
    Component.onCompleted: {
        sheepModel.populate();
    }

    XMLSheepModel { id: sheepModel }

    SilicaFlickable { id: flick
        anchors.fill: parent
        //contentHeight: pageHeader.height + view.height
        contentHeight: col.height

        Column{ id: col
            anchors.fill: parent
            width: parent.width

            PageHeader { id: pageHeader
                width: parent.width
                title: Qt.application.name
                property string flockname: ( /^1/.test(sheepModel.gen) ) ? qsTr("Flock of Gold! (%1)").arg(sheepModel.gen) : qsTr("Flock %1").arg(sheepModel.gen)
                description: flockname ? flockname + ("Sheep: %1").arg(view.count) : qsTr("No Sheep loaded");
            }

            ImageGridView { id: view
                clip: true
                height: mainPage.height - ( pageHeader.height + pageFooter.height )
                width: parent.width
                anchors {
                    //top: pageHeader.bottom
                    leftMargin: Theme.horizontalPageMargin/2
                    rightMargin: Theme.horizontalPageMargin/2
                }
                //footer: reuseFooter
                model: sheepModel
                // this is for the scroll label, which is empty otherwise
                dateProperty: "datetime"
                delegate: sheepThumb
                Component { id: sheepThumb
                    BackgroundItem {
                        width: GridView.view.cellWidth - Theme.paddingSmall
                        height: width
                        enabled: imgDelegate.status === Image.Ready

                        onClicked: {
                            const vu = videourl.substring(0, videourl.length - 2);
                            if (!app.settings.hires)
                                vu = vu.replace(".avi", ".mp4");
                            const imageurl = app.qserver + "gen/" + sheepModel.gen + "/" + sheepid + "/0.jpg"
                            pageStack.push(Qt.resolvedUrl("ImagePage.qml"), {"videourl": vu, "imageurl": imageurl, "generation": sheepModel.gen, "imgid": sheepid} )
                        }
                        Image {
                            id: imgDelegate
                            //source: Qt.resolvedUrl(thumburl)
                            source:  app.qserver + "gen/" + sheepModel.gen + "/" + sheepid + "/icon.jpg"
                            anchors.centerIn: parent
                            anchors.fill: parent
                            fillMode: Image.PreserveAspectCrop
                            smooth: false
                            cache: true
                            asynchronous: true
                            IconButton {
                                visible: imgDelegate.status == Image.Ready
                                anchors.bottom: parent.bottom
                                anchors.right: parent.right
                                height: parent.height /3
                                icon.source: "image://theme/icon-m-media-playlists?" + (down ? Theme.highlightColor : Theme.secondaryColor)
                                onClicked: {
                                    const vu = videourl.substring(0, videourl.length - 2)
                                    if (!app.settings.hires) 
                                        vu = vu.replace(".avi", ".mp4");
                                    pageStack.push(Qt.resolvedUrl("VideoPage.qml"), {"videourl": vu, "generation": sheepModel.gen, "imgid": sheepid} )
                                }
                            }
                        }
                        BusyIndicator { anchors.centerIn: parent; running: (imgDelegate.visible && imgDelegate.status === Image.Loading); size: BusyIndicatorSize.Medium; visible: running}
                    }
                }
                /*
            Component { id: reuseFooter
                ReUseTag {
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        topMargin: Theme.paddingSmall
                        bottomMargin: Theme.paddingSmall
                        leftMargin: Theme.horizontalPageMargin
                        rightMargin: Theme.horizontalPageMargin
                    }
                    width: GridView.view.width
                }
            }
            */
                ViewPlaceholder {
                    enabled: view.count == 0

                    text: qsTr("<p>There are no sheep loaded yet. Use the Pullup menu to get new sheep.</p>")
                    Label {
                        anchors.centerIn: parent
                        wrapMode: Text.Wrap
                        font.pixelSize: Theme.fontSizeLarge
                        horizontalAlignment: Text.AlignHCenter
                        color: Theme.secondaryColor
                        text: qsTr("<p>There are no sheep loaded yet. Use the Pullup menu to get new sheep.</p>")
                            + qsTr("<p>You need to be online for that.</p>")
                            + "<p>&nbsp;<br /></p>"
                            + qsTr("<p>If you have an Electric Sheep account, use the Pulldown menu to access the Settings and enter it there.</p>")
                            + "<p>&nbsp;</p>"
                            + qsTr("<p>Having an account is not required.</p>")
                    }
                }
            }
            ReUseTag { id: pageFooter
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    topMargin: Theme.paddingSmall
                    bottomMargin: Theme.paddingSmall
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                width: parent.width
            }
        }
        PullDownMenu {
            flickable: flick
            MenuItem{text: qsTr("About");                   onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))}
            MenuItem{text: qsTr("Settings");                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))}
            MenuItem{text: qsTr("Jobs");                    onClicked: pageStack.push(Qt.resolvedUrl("NewSheepPage.qml"))}
        }
        PushUpMenu {
            flickable: flick
            MenuItem {
                text: qsTr("Play all Sheep");
                enabled: view.count > 0
                onClicked: { pageStack.push(Qt.resolvedUrl("VideoPage.qml"), {"playlist": sheepModel.getPlayList(), "generation": sheepModel.gen, "imgid": qsTr("all")} ) }
            }
            MenuItem{ text: qsTr("Get new Sheep") ; enabled: mayRequest; onClicked: { sheepModel.getNewData() } }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
