import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Gallery 1.0
import "../components"

Page {
    QtObject{
        id: job
        property bool finished: false
        property int jobId: 0
        function save() {}
        function load() {}
        function get() {}
        function submit() {}
    }
    JobModel { id: jobModel }
    PullDownMenu {
        flickable: flick
        MenuItem { text: qsTr("Request new Job"); enabled: job.jobId != 0 }
        MenuItem { text: qsTr("Submit Result"); enabled: job.finished }
    }
    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            PageHeader { z: 10; id: header; title: qsTr("Jobs:") ; description: "" }
        }
    }
    ReUseTag {anchors.bottom: parent.bottom}
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
