import QtQuick 2.6
import QtMultimedia 5.6
import Sailfish.Silica 1.0
import Sailfish.Silica.Background 1.0
//import Sailfish.Share 1.0
import Nemo.Notifications 1.0

import "../components"
import "../javascript/common.js" as JS

//import Sailfish.Gallery 1.0
//import Sailfish.WebView 1.0
//import Sailfish.WebEngine 1.0
//import Sailfish.WebView.Popups 1.0


Page {
    id: page
    background: ColorBackground {
        color: "black"
    }
    property url imageurl: null
    property url videourl: null
    property Playlist playlist: null
    property string generation
    property string imgid
    // TODO: determine from Video.metaData.videoCodec or mediaType
    property string contentType: {
        var re = /\.avi$/
        if ( re.test(videourl.toString()))
            return "video/x-msvideo";
        re = /\.mp4$/
        if ( re.test(videourl.toString()))
            return "video/mp4";
        return "application/octet-stream";
    }

    Component.onCompleted: {
        if (playlist != null) {
            video.playlist = playlist;
            console.debug("setting playlist...");
        } else {
            // mp4 is for mobile...
            // handled in the model already:
            //video.source = Qt.resolvedUrl(page.videourl.toString().replace(".avi", ".mp4"));
            video.source = Qt.resolvedUrl(page.videourl.toString());
            console.debug("setting video...");
        }
        video.autoPlay = true;
        video.play();
    }
    onOrientationTransitionRunningChanged: { if(orientationTransitionRunning) { video.pause()} else { video.play() } }
    /*
    BusyLabel {
        // devault is l10n "Loading"
        text: if ( video.status == MediaPlayer.Buffering)  { qsTr("Buffering") }
        running: video.status == MediaPlayer.Loading || video.status == MediaPlayer.Buffering
    }
    */
    PushUpMenu {
        flickable: flick
        MenuItem { text: qsTr("Share..."); enabled: false
        /*
            onClicked: {
                share.title = qsTr("Sheep ID: %1").arg(page.imgid);
                //share.resources = [ { "data": Qt.btoa(video.mediaObject), "name": video.metaData.title} ] ;
                share.resources = [ page.videourl, page.imageurl ]
                share.trigger();
            }
            ShareAction { id: share
                //mimeType: page.contentType
                mimeType:  "text/x-url"
            }
        */
        }
        MenuItem { text: qsTr("Concatenate Sheep"); onClicked: JS.concatVideos(); }
        MenuItem { text: qsTr("Open Sheep"); onClicked: Qt.openUrlExternally(page.videourl) }
        MenuItem { enabled: false; text: qsTr("Download Sheep"); onClicked: Qt.openUrlExternally(page.videourl) }
    }
    /*
    // try to pre-cache the next video and swap the players:
    Component { id: oddVideo;  Video { autoLoad: true; }
    Component { id: evenVideo; Video { autoLoad: true; }
    Loader {
        id: precache
        asynchronous: true
        active: page.playlist != null
        //active: page.playlist != null && playlist.currentIndex <= playlist.itemCount
        //source: playlist.itemSource(playlist.nextIndex(1))
        onSourceChanged: console.debug("Loader loaded:" + sourceComponent.source)
    }
    */
    SilicaFlickable {
        id: flick
        anchors.fill: parent
        anchors.centerIn: parent
        //contentHeight: page.isPortrait ? Math.max ( lcol.height, rcol.height) : lcol.height + rcol.height
        contentHeight: flow.height
        //width: parent.width
        Flow {
            id: flow
            //width: parent.width
            anchors.fill: parent
            //anchors.centerIn: parent
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            //spacing: Theme.paddingSmall
            //leftPadding: page.isPortrait ? 0 : Theme.horizontalPageMargin
            //rightPadding: page.isPortrait ? 0 : Theme.horizontalPageMargin
            PageHeader {
                id: header;
                //width: parent.width
                visible: page.isPortrait
                title: qsTr("Flock:", "sheep generation") + " " + page.generation + " " + qsTr("Sheep:", "sheep ID") + " " + page.imgid;
                description: ""
            }
            Column {
                id: lcol
                width: page.isPortrait ? parent.width : parent.width * 2/3
                topPadding: page.isPortrait ? Theme.itemSizeMedium : 0
                bottomPadding: page.isPortrait ? Theme.itemSizeMedium : 0
                Video {
                    id: video
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: page.isPortrait ? Screen.width : height * (4/3)
                    height: page.isPortrait ? width / (4/3) : Screen.width
                    fillMode: VideoOutput.PreserveAspectFit
                    autoLoad: true
                    // not in Qt 5.6:
                    //loops: MediaPlayer.Infinite
                    onStatusChanged: {
                        header.description = metaData.title.replace(/ - .*$/,"")
                        if ( status === MediaPlayer.EndOfMedia) {
                            console.debug(JSON.stringify(metaData, null, 2))
                            loop();
                        }
                    }
                    function loop() {
                        if (playlist != null) {
                            console.debug("next!")
                            playlist.next();
                        } else {
                            if (seekable) { console.info("looping..."); seek(0); play() } else { console.warn("not seekable!") }
                        }
                    }

                    Notification { id: notification; summary: qsTr("An Error occurred:") + video.errorString  }
                    onErrorChanged: { if (error != MediaPlayer.NoError) { notification.publish() } }

                    BackgroundItem{ z:11; anchors.fill: parent; onClicked: (video.playbackState == MediaPlayer.PlayingState) ? video.pause() : video.play() }
                }
            }
            Column {
                id: rcol
                width: page.isPortrait ? parent.width : parent.width * 1/3
                spacing: Theme.paddingLarge
                bottomPadding: Theme.itemSizeMedium
                anchors.leftMargin: Theme.itemSizeMedium
                ProgressBar {
                    id: progressBar
                    width: parent.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    value: video.position
                    minimumValue: 0
                    maximumValue: video.duration
                    label:  video.status == MediaPlayer.Loading ? qsTr("Loading...") : qsTr("Progress")
                    indeterminate: video.status == MediaPlayer.Loading
                }
                ListView {
                    id: playlistList
                    clip: true
                    visible: page.playlist != null
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width - Theme.horizontalPageMargin * 4
                    height: Theme.itemSizeHuge*2
                    contentHeight: Theme.itemSizeHuge
                    spacing: 0
                    // content:
                    currentIndex: page.playlist.currentIndex
                    model: page.playlist
                    delegate: playlistEntry
                    highlight: highlightBar
                    highlightFollowsCurrentItem: true
                    //header: playlistProgressBar
                    //footer: labels
                    footerPositioning: ListView.OverlayFooter
                    headerPositioning: ListView.OverlayHeader

                    Component{ id: labels
                        Row {
                            z: 9
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: ListView.view.width
                            AboutLabel {
                                id: aboutLabel
                                width: parent.width/2
                                anchors.horizontalCenter: undefined
                                font.pixelSize: Theme.fontSizeSmall
                                text: link ? qsTr("<a href='%1'>Details</a>").arg(link)  : ""
                                link: video.metaData.title.toString().replace(/ - .*$/,"")
                                //text: video.metaData.title.toString().replace(/^[^-]* /,"")
                            }
                            AboutLabel {
                                id: licenseLabel
                                width: parent.width/2
                                anchors.horizontalCenter: undefined
                                font.pixelSize: Theme.fontSizeSmall
                                text: qsTr("<a href='%1'>License</a>").arg(video.metaData.comment.toString().replace("license:",""))
                            }
                        }
                    }
                    Component { id: playlistProgressBar
                        ProgressBar {
                            //visible: page.playlist != null
                            //width: parent.width
                            width: ListView.view.width
                            anchors.horizontalCenter: parent.horizontalCenter
                            value: video.playlist.currentIndex + 1
                            minimumValue: 0
                            maximumValue: video.playlist.itemCount
                            //label: qsTr("Playing %1/%2").arg(value).arg(maximumValue)
                            label: qsTr("%1/%2").arg(value).arg(maximumValue)
                            palette.highlightColor: Theme.highlightBackgroundColor
                        }
                    }
                    Component { id: highlightBar
                        Rectangle {
                            //width: ListView.view.width
                            color: Theme.rgba(Theme.highlightBackgroundColor, Theme.opacityFaint);
                            border.color: Theme.highlightBackgroundColor
                        }
                    }
                    Component { id: playlistEntry
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: ListView.view.width
                            //visible: (( index >= video.playlist.currentIndex) && ( index <= video.playlist.currentIndex + playlistList.maxChildren) )
                            font.pixelSize: ListView.isCurrentItem ? Theme.fontSizeMedium : Theme.fontSizeSmall
                            horizontalAlignment: Text.AlignHCenter
                            elide: Text.ElideRight
                            text: source.toString().replace(/^.*\//gi,"")
                            color: ListView.isCurrentItem ? Theme.primaryColor : Theme.secondaryColor
                            }
                    }
                }
            }
        }
    }
    ReUseTag {anchors.bottom: parent.bottom; visible: page.isPortrait}
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
