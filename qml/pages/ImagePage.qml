import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Gallery 1.0
import "../components"

Page {
    id: page
    property url imageurl
    property url videourl
    property string generation
    property string imgid
    PageHeader { z: 10; id: header; title: qsTr("Generation:", "sheep generation") + " " + parent.generation + " " + qsTr("Sheep:", "sheep ID") + " " + parent.imgid; description: imageurl }
    ImageViewer {
        id: viewer
        source: parent.imageurl
        anchors.fill: parent
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        onClicked: pageStack.replace(Qt.resolvedUrl("VideoPage.qml"), {"videourl": videourl,  "generation": app.sheepdata.generation, "imgid": sheepid} )
    }
    ReUseTag {z: 10; anchors.bottom: parent.bottom}
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
