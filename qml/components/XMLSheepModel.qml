import QtQuick 2.6
import QtMultimedia 5.6
import QtQuick.XmlListModel 2.0
import "../javascript/common.js" as F
import "../javascript/esapi.js" as API

XmlListModel {
    id: model

    property int gen: app.sheepdata.generation;

    property string qserver: app.qserver;
    property string rserver: app.rserver;
    property string vserver: app.vserver;

    // Playlist property to convert ListModel to Playlist
    property Playlist playlist: Playlist {
        playbackMode: Playlist.Loop
    }

    onStatusChanged: {
        switch (status) {
            case XmlListModel.Null: 
                console.debug(("model status: %1").arg("Null"));
                break;
            case XmlListModel.Ready: 
                console.debug(("model status: %1").arg("Ready"));
                console.debug(("model elements: %1").arg(count));
                console.debug(("model gen: %1").arg(gen));
                //gen = get(0).generation;
                break;
            case XmlListModel.Loading: 
                console.debug(("model status: %1").arg("Loading"));
                break;
            case XmlListModel.Error: 
                console.debug(("model status: %1").arg("Error"));
                break;
        }
    }

    onXmlChanged: {
       // brute force get the gen attribute from the XML string:
       const fline = xml.split("\n")[1]; // <list gen="248" size="800 592" retry="600">
       const tag = fline.split(" ")[1]; // 'gen="248"'
       const val = tag.split("\"")[1]; // '248'
       gen= val;
    }
    //   <list gen="244" size="800 592" retry="600">
    //   <sheep id="27045" type="0" state="done" time="1283912914" size="1688352" rating="102" first="27045" last="27045" url="http://v2d7c.sheepserver.net.nyud.net/gen/244/27045/sheep.avi"/>
    //   <sheep id="33860" type="0" state="done" time="1291465044" size="2513326" rating="91" first="33259" last="27045" url="http://v2d7c.sheepserver.net.nyud.net/gen/244/33860/sheep.avi"/>
    //   ...
    //   </list>
    //query: "/list/"
    query: "/list/sheep"
    //XmlRole { name: "generation"; query: "@gen" }
    //XmlRole { name: "generation"; query: "parent::*/@gen/number()"; }
    XmlRole { name: "sheepid";    query: "@id/number()";     isKey: true; }
    XmlRole { name: "time";       query: "@time/number()";   isKey: true; }
    /* 
     * we need a real date value for the ImageGrid scroll bar. It will be accessed by model.item["name"] so we can't format it later 
     * so, convert the timestamp into a real date.
     * by getting the epoch (dateTime(foo)) and adding a number of seconds  (dayTimeDuration(PT + seconds + S)
     */
    XmlRole { name: "datetime";   query: "xs:dateTime('1970-01-01T00:00:00-00:00') + xs:dayTimeDuration(concat('PT', xs:integer(@time/number()), 'S'))" }
    XmlRole { name: "rating";     query: "@rating/number()"; }
    XmlRole { name: "first";      query: "@first/number()";  }
    XmlRole { name: "last";       query: "@last/number()";   }
    XmlRole { name: "videourl";   query: "@url/string()"     }

    function populate(){
        // load saved data unless we don't have some
        console.time("Populated model in");
        if (app.sheepdata.rawdata.length >= 0){
            console.debug("Modeldata: Loading from cache.");
            fillFromRaw(app.sheepdata.rawdata);
        } else {
            if (app.online) {
                console.debug("Modeldata: Loading from server.");
                getNewData();
            } else {
                console.warn("Modeldata: Trying to load data from server while network is offline.")
            }
        }
        console.timeEnd("Populated model in");
    }

    function savePlaylistData(data) {
        app.sheepdata.setValue("playlist", JSON.stringify(data));
    }

    function savePlaylistFile() {
        if ( (playlist != null) && (playlist.itemCount > 0) ) {
            console.debug("Saving Playlist: " + app.playlistFile);
            if (!playlist.save("file://" + app.playlistFile)) {
                console.log("Playlist Save Failed: " + playlist.error  + ", " + playlist.errorString)
            }
        }
    }

    function getPlayList() {
        playlist.clear();
        var p = [];
        for (var i = 0; i < model.count; i++) {
            var o = model.get(i);
            if (app.settings.hires) {
                playlist.addItem(o.videourl);
                p.push(o.videourl);
            } else {
                playlist.addItem(o.videourl.replace(".avi", ".mp4"));
                p.push(o.videourl.replace(".avi", ".mp4"));
            }
        }
        //savePlaylistFile();
        savePlaylistData(p);
        return playlist;
    }

    function fillFromRaw(data) {
        xml = '<?xml version="1.0"?>\n' + data;
        reload();
    }

    function getNewData() {
        if (!app.mayRequest) {
            console.warn("New Request sooner than Retry limit. Not doing it.");
            return;
        }
        if (model.qserver.length <= 0) {
            console.warn("No Query Server selected. Can't query anyone.");
            return;
        }
        backoffTimer.restart();
        console.time("loaded new data in");
        const account = app.settings.account;
        const hash = app.settings.hash;
        const version = Qt.application.version;
        //"$HOST/cgi/list?u=$UID&v=$VERSION" | xq '.list.sheep[]."@url" '
        const requestUrl = app.qserver + "cgi/list";
        var query = Qt.resolvedUrl(requestUrl);
        query = query + "?";
        query = query + "&u=" + encodeURIComponent(account);
        query = query + "&v=" + encodeURIComponent(version);
        console.debug("Query string:", query);

        var r = new XMLHttpRequest()
        r.open('GET', query);
        r.setRequestHeader('User-Agent', "Mozilla/5.0 (Sailfish 4.0; Mobile) " + Qt.application.name + "/" + Qt.application.version);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.responseType = 'arraybuffer'; //we need real binary gzip data
        r.send();

        r.onreadystatechange = function(event) {
          if (r.readyState == XMLHttpRequest.DONE) {
              /*
               * response is a blob of gzipped XML file, unzip it
               * it is NOT just a gzipped response!
               */
              const payload = F.unzipResponse(r.response);
              // save as cache for next time:
              app.sheepdata.setValue("rawdata", payload);
              fillFromRaw(payload);
          }
        }
        // save time queried so we can't repeat too soon:
        app.queried = Math.floor( Date.now() / 1000 );
        console.timeEnd("loaded new data in");
    }
}
// vim: ft=javascript expandtab ts=4 sw=4 st=4
