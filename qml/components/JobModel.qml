import QtQuick 2.6
import "../javascript/common.js" as F
import "../javascript/esapi.js" as API

ListModel {
    id: model

    property string rserver: app.settings.rserver
    property int retry: 600     // query backoff time, updated from server

    // deal with async server selection:
    onRserverChanged: {
        if (rserver.length <= 0) {
            rserver = API.selectRserver();
        } else {
            ready = true;
            console.debug("Render Server selected: " + rserver);
        }
    }

    // Get A Rendering Job
    function getJob() {
        if (!app.mayRequest) {
            console.warn("New Request sooner than Retry limit. Not doing it.");
            return;
        }
        /*
        GET $HOST/cgi/get?n=$NICK&w=$URL&v=$VERSION&u=$UID&r=1&c=1

        $NICK is the account name (nick name) of the user.
        $URL is the credit URL provided by the user.
        r and c are both hardcoded to 1 in the client but they are ignored by the server.
        */
        var account = app.settings.account;
        var version = Qt.application.version;
        var account = app.settings.appuid;

        var requestUrl = qserver + "cgi/get";
        var query = Qt.resolvedUrl(requestUrl);
        query = query + "?";
        query = query + "&n=" + encodeURIComponent(account);
        query = query + "&w=" + encodeURIComponent("http://creativecommons.org/licenses/by-nc/3.0/us/")
        query = query + "&v=" + encodeURIComponent(version);
        query = query + "&u=" + encodeURIComponent(uid);
        query = query + "&r=1&c=1";
        console.debug("Query string:", query);

        var r = new XMLHttpRequest()
        r.open('GET', query);
        r.setRequestHeader('User-Agent', "Mozilla/5.0 (Sailfish 4.0; Mobile) " + Qt.application.name + "/" + Qt.application.version);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.responseType = 'arraybuffer'; //we need real binary gzip data
        r.send();

        r.onreadystatechange = function(event) {
            if (r.readyState == XMLHttpRequest.DONE) {
                /*
                 * response is a blob of gzipped XML file, unzip it
                 * it is NOT just a gzipped response!
                 *
                 */
                var payload = F.unzipResponse(r.response);
                // save as cache for next time:
                app.sheepdata.setValue("rawdata", payload);
                fillFromRaw(payload);
            }
        }
        // save time queried so we can't repeat too soon:
        app.queried = Math.floor( Date.now() / 1000 );
    }

    function fillFromRaw(data){
        /*
    The response is gzipped XML
    <get gen="12" id="1" type="0" prog="flame" job="3" frame="3">
    <args bits="33" jpeg="100" />
    followed by the output from one of these

         * if loop: env rotate=$spex_file enclosed=0 frame=$frame_id nframes=$nframes /usr/bin/flam3-genome
         * if edge: env inter=$spex_file enclosed=0 frame=$frame_id nframes=$nframes /usr/bin/flam3-genome

     And finished off by
    </get>

*/
    }
}
// vim: ft=javascript expandtab ts=4 sw=4 st=4
