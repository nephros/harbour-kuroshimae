import QtQuick 2.6
import Sailfish.Silica 1.0
Label {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    textFormat: Text.StyledText
    horizontalAlignment: Text.AlignHCenter
    wrapMode: Text.Wrap
    linkColor: Theme.highlightColor
    property url link
    onLinkActivated: Qt.openUrlExternally(link)
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
