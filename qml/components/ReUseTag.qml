import QtQuick 2.6
import Sailfish.Silica 1.0

AboutLabel {
    height: Theme.itemSizeSmall
    color: Theme.secondaryColor
    font.pixelSize: Theme.fontSizeTiny
    link: Qt.resolvedUrl("https://electricsheep.org/license/");
    text: qsTr("Electric Sheep") + " " + qsTr("images and animations distributed under a CC BY-NC licence and subject to the <a href='%1'>reuse policy</a>.").arg(link.toString())
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
