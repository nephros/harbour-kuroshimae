# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       harbour-kuroshimae

# >> macros
# << macros

Summary:    Sheep of the Sea!
Version:    0.9.7
Release:    0
Group:      Amusements
License:    ASL 2.0
BuildArch:  noarch
URL:        https://gitlab.com/nephros/harbour-kuroshimae
Source0:    %{name}-%{version}.tar.gz
Source100:  harbour-kuroshimae.yaml
Source101:  harbour-kuroshimae-rpmlintrc
Requires:   sailjail
Requires:   libsailfishapp-launcher
Requires:   nemo-qml-plugin-configuration-qt5
Requires:   sailfish-components-gallery-qt5
Requires:   qt5-qtdeclarative-import-multimedia
Requires:   (yq or python3-yq)
Requires:   curl
Requires:   ffmpeg-tools
BuildRequires:  qt5-qttools-linguist
BuildRequires:  qt5-qmake
BuildRequires:  sailfish-svg2png
BuildRequires:  qml-rpm-macros
BuildRequires:  desktop-file-utils

%description
%if "%{?vendor}" == "chum"
PackageName: Kuroshimae
Type: desktop-application
DeveloperName: nephros
Categories:
 - Media
 - Video
Custom:
  Repo: %{url}
Icon: %{url}/raw/master/icons/harbour-kuroshimae-512.png
Screenshots:
    - %{url}/raw/master/screenshot_001.png
    - %{url}/raw/master/screenshot_002.png
    - %{url}/raw/master/screenshot_003.png
Url:
  Donation: https://noyb.eu/en/donations-other-support-options
%endif


%prep
%setup -q -n %{name}-%{version}

# >> setup
# << setup

%build
# >> build pre
# << build pre

%qmake5 

make %{?_smp_mflags}

# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre
%qmake5_install

# >> install post
# mangle version information
sed -i -e 's/Qt\.application\.version = "unreleased";$/Qt.application.version = "%{version}";/' %{buildroot}%{_datadir}/%{name}/qml/%{name}.qml
# << install post

desktop-file-install --delete-original       \
  --dir %{buildroot}%{_datadir}/applications             \
   %{buildroot}%{_datadir}/applications/*.desktop

%files
%defattr(-,root,root,-)
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/tools/*
%{_datadir}/%{name}/qml/*
%{_datadir}/%{name}/translations/*
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/applications/%{name}.desktop
# >> files
# << files
