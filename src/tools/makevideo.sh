#!/usr/bin/env bash

SHEEP_CACHE_DIR=$HOME/.cache/sheep/
SHEEP_DIR=$HOME/Videos/ElectricSheep/
DCONF_ROOT=/org/nephros
DCONF_APP=Kuroshimae

if [ ! -z "$DEBUG" ]; then
  DCONF_APP=QtQmlViewer
fi

DCONF_LOC=${DCONF_ROOT}/${DCONF_APP}

GEN=$( dconf read ${DCONF_LOC}/sheepdata/generation | sed "s/\..*$//" )
PLS=$( dconf read ${DCONF_LOC}/sheepdata/playlist | sed "s/^'//;s/'$//" | jq .[] )
echo generation: $GEN
echo read playlist: $PLS

if [ -e "${SHEEP_DIR}"/"${GEN}".mkv ]; then
    echo "Generation $GEN already created, exiting."
    exit 0
fi

if [ -e ${SHEEP_CACHE_DIR} ] ; then rm -rf ${SHEEP_CACHE_DIR}; fi
echo $PLS | xargs curl -L --create-dirs --output-dir "${SHEEP_CACHE_DIR}/${GEN}" --progress-bar --remote-name-all
ffmpeg -f concat -safe 0 -i <(for f in "${SHEEP_CACHE_DIR}"/"${GEN}"/*.mp4; do echo "file '$f'"; done) -c copy "${SHEEP_DIR}"/"${GEN}".mkv
