echo -n "reset sheepdata and query server settings? "
read a
if [  x"$a" = x"y" ]; then
dconf reset -f /org/nephros/QtQmlViewer/sheepdata/
dconf reset /org/nephros/QtQmlViewer/queried
dconf reset /org/nephros/QtQmlViewer/qserver
dconf reset /org/nephros/QtQmlViewer/rserver
dconf reset /org/nephros/QtQmlViewer/vserver
echo ok.
else
echo no.
fi

echo -n "reset app preferences? "
read a
if [  x"$a" = x"y" ]; then
dconf reset /org/nephros/QtQmlViewer/hires
echo ok.
else
echo no.
fi

echo -n "reset account info? "
read a
if [  x"$a" = x"y" ]; then
dconf reset /org/nephros/QtQmlViewer/account
dconf reset /org/nephros/QtQmlViewer/appid
dconf reset /org/nephros/QtQmlViewer/hash
echo ok.
else
echo no.
fi

