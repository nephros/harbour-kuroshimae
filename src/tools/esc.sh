#!/usr/bin/env bash

VERSION=0.9
ACCOUNT=nephros
SALT=sh33p
UUID=889C892ABDB23CA7
USERNAME=$1
PASSWD=$2

SHEEP_CACHE_DIR=~/.cache/sheep/
SHEEP_DIR=~/Videos/ElectricSheep

get_hash() {
    printf '%s%s%s' $PASSWD $USERNAME $SALT | md5sum |  cut -d "-" -f1
}

log() {
	printf "%s\n" $* >> ~/.sheep.log
}

HASH=$(get_hash)

## query redirect url
re=$(curl -s -L "http://community.sheepserver.net/query.php?q=redir&u=$ACCOUNT&p=$HASH&v=$VERSION&i=$UID")

# answers:  <query><redir role="none" host="http://v3d0.sheepserver.net/" vote="http://v3d0.sheepserver.net/" render="http://v3d0.sheepserver.net/"/></query>
# TODO: parse reponse
HOST="http://v3d0.sheepserver.net/"
VOTE="http://v3d0.sheepserver.net/"
RENDER="http://v3d0.sheepserver.net/"

# get list of current sheep
jsonpath='.list.sheep[]."@url"'
# curl -s --compressed -L "$HOST/cgi/list?u=$UID&v=$VERSION" | xq '.list.sheep[]."@url" '
# step-by-step:
# echo res:
# res=$(curl -s -L "$HOST/cgi/list?u=$UID&v=$VERSION" |  gunzip -c)
# echo rej:
# rej=$(echo $res | xq)
# echo final:
# echo $rej | jq '.list.sheep[]."@url" '

echo "Getting new sheep"...
# the response *data* is gzip compressed, so `curl --compressed` is not enough:
jlist=$(curl -s -L "$HOST/cgi/list?u=$UID&v=$VERSION" | gunzip -c )
log $jlist
genjsonpath='.list."@gen"'
gen=$(echo $jlist | jq ${genjsonpath} )

if [ -e "${SHEEP_DIR}"/"${gen}".mkv ]; then
	echo "Generation $gen already created, exiting."
	exit 0
fi

echo $jlist| xq $jsonpath \
		| sed 's/\.avi/.mp4/' \
		| tee "${SHEEP_CACHE_DIR}"/list.json \
		| xargs curl -L --create-dirs --output-dir "${SHEEP_CACHE_DIR}/${gen}" --progress-bar --remote-name-all


ffmpeg -f concat -safe 0 -i <(for f in "${SHEEP_CACHE_DIR}"/"${gen}"/*.mp4; do echo "file '$f'"; done) -c copy "${SHEEP_DIR}"/"${gen}".mkv

