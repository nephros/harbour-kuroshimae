<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="20"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="30"/>
        <source>What&apos;s %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="36"/>
        <source>%1 is an unofficial ElectricSheep client for Sailfish OS.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="39"/>
        <source>What&apos;s an Electric Sheep?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>&lt;p&gt;Electric Sheep is a collaborative abstract artwork run by thousands of people all over the world. When their computers &apos;sleep&apos;, the Electric Sheep comes on and the computers communicate with each other by the internet to share the work of creating morphing abstract animations known as &apos;sheep&apos;.&lt;/p&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="47"/>
        <source>&lt;p&gt;&lt;a href=&apos;%1&apos;&gt;electricsheep.org&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="78"/>
        <source>%1 is free software released under the &lt;a href=&apos;%2&apos;&gt;Apache License 2.0&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="81"/>
        <source>%1 uses components licensed under MIT and LGPL 2.1 licenses&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="92"/>
        <source>&lt;a href=&apos;%1&apos;&gt;Creative Commons By-NC 3.0&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="98"/>
        <source>Sources for %1 &lt;a href=&apos;%2&apos;&gt;available on GitLab&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="102"/>
        <source>Sources for the original %1 client and the fractal flame renderer &lt;a href=&apos;%2&apos;&gt;available on Github&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="102"/>
        <location filename="../qml/pages/AboutPage.qml" line="118"/>
        <source>Electric Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="118"/>
        <source>%1 by &lt;a href=&apos;%2&apos;&gt;Scott Draves and others&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="49"/>
        <source>Why the name?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>So, this is an image of &lt;i&gt;Costasiella kuroshimae&lt;/i&gt;, a mollusc who lives in the sea:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="75"/>
        <source>Licenses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="95"/>
        <source>Source Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="105"/>
        <source>Developers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="24"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImagePage</name>
    <message>
        <location filename="../qml/pages/ImagePage.qml" line="12"/>
        <source>Sheep:</source>
        <comment>sheep ID</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/ImagePage.qml" line="12"/>
        <source>Generation:</source>
        <comment>sheep generation</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="80"/>
        <location filename="../qml/pages/MainPage.qml" line="113"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="81"/>
        <location filename="../qml/pages/MainPage.qml" line="114"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="86"/>
        <location filename="../qml/pages/MainPage.qml" line="107"/>
        <source>Play all Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="108"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="87"/>
        <location filename="../qml/pages/MainPage.qml" line="110"/>
        <source>Get new Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="72"/>
        <source>&lt;p&gt;There are no sheep loaded yet. Use the Pullup menu to get new sheep.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="73"/>
        <source>&lt;p&gt;You need to be online for that.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="82"/>
        <location filename="../qml/pages/MainPage.qml" line="115"/>
        <source>Jobs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="40"/>
        <source>Flock of Gold! (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="35"/>
        <source>Last updated: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="42"/>
        <source>Flock %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="45"/>
        <source>No Sheep loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="75"/>
        <source>&lt;p&gt;If you have an Electric Sheep account, use the Pulldown menu to access the Settings and enter it there.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="77"/>
        <source>&lt;p&gt;Having an account is not required.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewSheepPage</name>
    <message>
        <location filename="../qml/pages/NewSheepPage.qml" line="19"/>
        <source>Request new Job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/NewSheepPage.qml" line="20"/>
        <source>Submit Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/NewSheepPage.qml" line="28"/>
        <source>Jobs:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReUseTag</name>
    <message>
        <location filename="../qml/components/ReUseTag.qml" line="9"/>
        <source>Electric Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ReUseTag.qml" line="9"/>
        <source>images and animations distributed under a CC BY-NC licence and subject to the &lt;a href=&apos;%1&apos;&gt;reuse policy&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="13"/>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="17"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="25"/>
        <source>Prefer High-Res Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="26"/>
        <source>Download larger and high quality videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="38"/>
        <source>Account Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="54"/>
        <source>Account credentials are not saved, only a salted hash is. Still, this is insecure, so don&apos;t use valuable credentials here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="62"/>
        <source>Already Configured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="63"/>
        <source>Tap this switch to change username and password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="71"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="104"/>
        <source>The App ID is required by the Electric Sheep web services. It is supposed to be generated at application install only, but for the sake of privacy, you can reset it here. This shouldn&apos;t ususally be necessary though, and the effects of doing so are unknown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="111"/>
        <source>App Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="116"/>
        <source>Regenerated %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="32"/>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="97"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="62"/>
        <source>Share...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="76"/>
        <source>Concatenate Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="77"/>
        <source>Open Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="78"/>
        <source>Download Sheep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="114"/>
        <source>Sheep:</source>
        <comment>sheep ID</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="114"/>
        <source>Flock:</source>
        <comment>sheep generation</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="147"/>
        <source>An Error occurred:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="166"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="166"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="199"/>
        <source>&lt;a href=&apos;%1&apos;&gt;Details&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="208"/>
        <source>&lt;a href=&apos;%1&apos;&gt;License&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="222"/>
        <source>%1/%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
