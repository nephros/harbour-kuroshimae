TEMPLATE = aux
# Configures svg to png
THEMENAME = sailfish-default
INSTALLS += appicon

appicon.sizes = \
    48 \
    64 \
    86 \
    108 \
    128 \
    172 \
    256 \
    512 \

for(iconsize, appicon.sizes) {
    profile = $${iconsize}x$${iconsize}
    srcicon = $$TARGET-$${iconsize}.png

    appicon.commands += install -D -m 644 $${_PRO_FILE_PWD_}/icons/$${srcicon} $${OUT_PWD}/icons/$${profile}/apps/$${TARGET}.png &&
    appicon.files += $${OUT_PWD}/icons/$${profile}
}
appicon.commands += true
appicon.path = $$PREFIX/share/icons/hicolor/

